package com.spikedhand.doodleEscape 
{
	import asset.ExitLogo;
	import asset.sound.PaperRip;
	import flash.display.MovieClip;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author ...
	 */
	public class Exit extends MovieClip 
	{
		public static const  LEVEL_DONE:String = "LEVEL_DONE";
		private var map:Map;
		
		public function Exit(map:Map) {
			super();
			this.map = map;
			this.addChild(new ExitLogo());
			this.addEventListener(Event.EXIT_FRAME, detect);
			
		}
		
		private function detect(e:Event):void {
			if(map.player != null){
				if (this.hitTestObject(map.player)) {
					this.removeEventListener(Event.EXIT_FRAME,detect);
					trace("Level Done");
					new PaperRip().play();
					this.dispatchEvent(new Event(LEVEL_DONE));
				}
			}
		}
		
		
	}

}