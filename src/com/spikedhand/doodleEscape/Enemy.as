package com.spikedhand.doodleEscape 
{
	import asset.RollingEnemy;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.utils.getQualifiedClassName;

	/**
	 * ...
	 * @author 
	 */
	public class Enemy extends MovieClip
	{
		private static var moveRate:Number = 0;
		
		private var map:Map,
					queuedX:int,
					queuedY:int;
					
		protected var movingX:int,
						movingY:int;
		
		public function Enemy(map:Map) {
			this.map = map;
			
			//Prep
			movingX = 0;
			movingY = 0;
			this.addEventListener(Event.ENTER_FRAME, tick);
		}
		
		public function horizontalMove(blocks:Number):void {
			queuedX-= blocks;
		}
		public function verticalMove(blocks:Number):void {
			queuedY-=blocks;
		}
		
		//-------------------------------------------Private
		private function tick(e:Event):void {
			//Has anyone caluated the how much to move per frame
			if (moveRate == 0) {
				//every second move one block
				moveRate = 64 / stage.frameRate;
			}

			//Add queued movement
			movingX += (queuedX * 64);
			movingY += (queuedY * 64);
			
			queuedX = 0;
			queuedY = 0;
			//Move the actually character on the X
			if (movingX > 0) {
				this.x -= moveRate;
				movingX-=moveRate;
			}else if (movingX < 0) {
				this.x += moveRate;
				movingX+=moveRate;
			}
			//Move the actually character on the Y
			if (movingY > 0) {
				this.y -= moveRate;
				movingY-=moveRate;
			}else if (movingY < 0) {
				this.y += moveRate;
				movingY+=moveRate;
			}
			
			if(map.player != null){
				//Did I hit player?
				if (hitTestObject(map.player) && !(map.player.isDead || map.player.isDying)) {
					trace("Player killed by " + getQualifiedClassName(this));
					map.player.die();
				}
			}
		}
		
		
	}

}