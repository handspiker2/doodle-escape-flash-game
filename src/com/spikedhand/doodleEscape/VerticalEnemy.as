package com.spikedhand.doodleEscape 
{
	import asset.VerticalEnemy;
	import flash.events.Event;

	public class VerticalEnemy extends Enemy 
	{
		
		private var map:Map;
		private var currentDirection:int;
		
		public function VerticalEnemy(map:Map) {
			this.map = map;
			super(map);
			this.addChild(new asset.VerticalEnemy());
			this.addEventListener(Event.EXIT_FRAME, collustion);
			
			//Start the rolling
			this.currentDirection = -1;
		}
		
		private function collustion(e:Event):void {
			
			var x:Number = Math.floor(this.x / 64),
				y:Number = Math.floor(this.y / 64);
			//If no more moving
			if(this.movingY == 0){
				if (map.getMapTile(x, y+ 2) is Block) {
					//trace("hit on Down");
					currentDirection = -1;
				}else if (map.getMapTile(x, y -1) is Block) {
					//trace("hit on Top");
					currentDirection = 1;
				}
			
				this.verticalMove(currentDirection);
			}
		}
		
	}

}