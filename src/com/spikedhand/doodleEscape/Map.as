package com.spikedhand.doodleEscape 
{
	import flash.display.Sprite;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.utils.getQualifiedClassName;
	import flash.display.DisplayObject;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.events.ErrorEvent;
	/**
	 * ...
	 * @author Devin Handspiker-Wade
	 */
	public class Map  extends MovieClip
	{
		//CONST for error event
		public static const ERROREVENT:String = "URLerrorEvent";
		
		private var level:Array;
		private var _player:Player;
		private var loader:URLLoader;
		private var _lives:int;
		
		public function Map() {
			//pre-create the loader
			loader = new URLLoader();
			
			//Add events						
			loader.addEventListener(Event.COMPLETE, urlLoaded);
			loader.addEventListener(IOErrorEvent.IO_ERROR, loaderError);

			//Create array to keep the map
			level = [];
			//Add event handler
			this.addEventListener(Event.ENTER_FRAME, onFrame);		
			
			_player = null;
			_lives = 3;
		}
		
		//------------------------------Add Object
		public function addByURL(url:URLRequest):void {
			//Request the data
			loader.load(url);
		}
		
		
		public function addByString(layout:String):void {			
			var data:Array = layout.split("");
			var x:Number = 0,
				y:Number = 0;
				
			for each (var letter:String in data) {
				//Add objects
				if (letter == "p") {
					this.add(new Player(this), x, y);
				}else if (letter == "b") {
					this.add(new Block(), x, y);
				}else if (letter == "k") {
					this.add(new KillerTile(this), x, y);
				}else if (letter == "R") {
					this.add(new RollingEnemy(this), x, y);
				}else if (letter == "V") {
					this.add(new VerticalEnemy(this), x, y);
				}else if (letter == "E") {
					this.add(new Exit(this), x, y);
				}
				
				//detect position
				if (letter == "\n") {
					y++;
					x = 0;
				}else {
					x++;
				}
			}
		}
		//------------------------------Add Objects
		public function add(child:DisplayObject, x:Number, y:Number):void {
						
				if (this.contains(child)) {
					this.removeChild(child);
					
					for (var i:int = 0; i > level.length; i++) {
						for (var k:int = 0; k > level[i].length; k++) {
							if (level[i][k] == child) {
								level[i][k] = null;
							}
						}
					}
					
				}
			
			//create layer if is doesnt exist
			if (level[x] == null) {
				level[x] = [];
			}
		
			if (getQualifiedClassName(child) != getQualifiedClassName(Player)) {

				//add to map
				level[x][y] = child;
			}else {
				//Reset the map position 
				this.x = 0;
				this.y = 0;
				//Added the player (AKA not locked to grid)
				trace("Added player");
				//TypeCasting to player
				trace ( "Player created at (" + x + ")(" + y + ")");
				_player = Player(child);
				//Tell about starting position
				_player.startX = x;
				_player.startY = y;
				
				this.x -= (x * 64) - 300;
				this.y -= (y * 64) - 300;
			}
			this.addChild(child);
			child.x = (x * 64);
			child.y = (y * 64);
			
		}
		
		public function clear():void {
			trace("start clear");
			if (_player != null) {
				this.removeChild(_player);
				_player = null;
			}
			
			// scroll through x values
			for (var x:int = 0; x < level.length; x++) {
				if (level[x] != null) {
					//scroll though y values
					for (var y:int = 0; y < level[x].length;y++ ) {
						if (level[x][y] != null) {
							//Remove from stage
							this.removeChild(level[x][y]);
							//Destory the refence
							level[x][y] = null;
						}
					}
				}
			}
			trace("done clear");
		}
		public function resetPlayer() : void {
			var x:Number = Math.round(_player.startX),
				y:Number =  Math.round(_player.startY);

			//Remove player
			this.removeChild(_player);
			_player = null;
			
			this.add(new Player(this), x, y);
		}
		
		//-------------------------GETS
		public function getMapTile(x:Number, y:Number):DisplayObject {
			
			if ((x < 0) || (y < 0)) {
				return null;
			}
			if ((level[x] == null)) {
				return null;
			}
			return level[x][y];
		}
		
		public function get player():Player {
			return _player;
		}
		
		public function get lives():int {
			return _lives;
		}
		
		//------------------------sets
		
		public function set lives(val:int):void {
			_lives = val;
		}
		
		//-----------------------------------------Events
		
		private function urlLoaded(e:Event):void {
		
			var data:String = loader.data;
			this.addByString(data);
			this.dispatchEvent(new Event("LEVELLOADED"));
			loader.close();
		}
		
		private function onFrame(e:Event):void {
			if (player != null) {
				
				if (_player.isDead) {
					this.resetPlayer();
					return;
				}
				
				if (player.walking != 0) {
					player.x += (player.walking * Player.WALK_RATE);
					this.x -= (player.walking * Player.WALK_RATE);
				}
				
				if (player.isJumping) {
					this.y += (Player.FALL_RATE * 1.7);
					player.y -= (Player.FALL_RATE *1.7);
				}
												
				if (player.isFalling) {
					this.y -= Player.FALL_RATE;
					player.y += Player.FALL_RATE;
				}
				
				//Detect off map
				var x:int = Math.round(player.x / 64),
					y:int = Math.round(player.y / 64);
				
				if (level[x][y] is KillerTile) {
						player.die();
				}
			}
		}
		
		private function loaderError(e:Event):void {
				//Output error
				trace("No Level found"); 			
				//Throw event if the file doesnt exist
				this.dispatchEvent(new Event(ERROREVENT, true));
			}
	}

}