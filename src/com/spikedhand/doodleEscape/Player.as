package com.spikedhand.doodleEscape 
{
	import asset.sound.Fail;
	import flash.display.DisplayObject;
	import flash.display.MovieClip;
	import flash.display.Sprite;
	import com.spikedhand.doodleEscape.*;
	import asset.*;
	import flash.events.ErrorEvent;
	import flash.events.Event;
	import flash.events.TimerEvent;
	import flash.utils.Timer;
	/**
	 * ...
	 * @author Devin Handspiker-Wade
	 */
	public class Player extends MovieClip{
		public static const WALK_LEFT:int = -1,
							WALK_RIGHT:int = 1, 
							FALL_RATE:Number = 10, 
							WALK_RATE:Number = 7,
							JUMP_TIME:int = 350;
		
		//Other global variables
		private var map:Map;
		private var clock:Timer;
		//Clips
		private var stillClip:StillPlayer, walkingClip:WalkingPlayer, deathClip:DyingPlayer;
		
		//Props
		private var _walking:Number,
					_dying:Boolean,
					_jumping:Boolean,
					_currentClip:MovieClip,
					_dead:Boolean,
					_startX:Number,
					_startY:Number;
		//-----------------------------------------------Constructor
		public function Player(map:Map) {
			//Grab map
			this.map = map;
			//Default Props
			_walking = 0;
			_dying = false;
			_jumping = false;
			_dead = false;
			
			//Create needed object
			clock = new Timer(Player.JUMP_TIME);
			
			//Create Animations
			stillClip = new StillPlayer();
			walkingClip = new WalkingPlayer();
			deathClip = new DyingPlayer();
			
			//Add first display
			this.addChild(stillClip);
			
		}
		
		//-------------------------------------------Public Methods
		public function die():void {
			new Fail().play();
			this.walking = 0;
			MovieClip(this.getChildAt(0)).stop();
			this.removeChild(this.getChildAt(0));
			this.addChild(deathClip);
			MovieClip(this.getChildAt(0)).play();
			this.addEventListener("PlayerDeathDone", death,true);
			
			_dying = true;
					
		}
		
		public function jump():void {
			var touching:Boolean = touchingBlock(Math.round(this.x / 64),Math.round(this.y / 64)+2)
			
			if(touching){
				_jumping = true;
			
				clock.start();
				
				clock.addEventListener(TimerEvent.TIMER, doneJump );
			}
		}
			
		//-----------------------------------------Private Method
		private function death(e:Event):void {
			trace("Player lost a life");
			MovieClip(this.getChildAt(0)).stop();
			this.removeEventListener("PlayerDeathDone", death);
			this.removeChild(this.getChildAt(0));
			
			_dying = false;
			_dead = true;
			//Remove a Life
			map.lives--;
			trace("lives: " + map.lives);
			if (map.lives <= 0) {
				trace("sending gameover");
				this.dispatchEvent(new Event("GAMEOVER"));
			}else {
							
			map.resetPlayer();
			}

		}
		private function doneJump(e:Event):void {
				clock.stop();
				clock.removeEventListener(TimerEvent.TIMER, doneJump);
				_jumping = false;
		}
		
		private function touchingBlock(x:Number, y:Number):Boolean {
			var tile:DisplayObject = map.getMapTile(x,y);
				//trace("PlayerX = " + floorX + "Y= " + floorY + "tile= " +tile);
				if (tile != null) {
					//Collistion detection
					if ((this.hitTestObject(tile)) && (tile is com.spikedhand.doodleEscape.Block)) {
						return true;
					}
				}
				return false;
		}
			
		//-----------------------------------------------READONLY Props
		
		public function get isDying():Boolean{
			return _dying;
		}
		public function get isDead():Boolean{
			return _dead;
		}
		public function get isJumping():Boolean {
			if (map.getMapTile(Math.round(this.x / 64), Math.ceil(this.y / 64) - 1) is com.spikedhand.doodleEscape.Block) {
				return false;
			}
			return _jumping;
		}

		public function  get isFalling():Boolean {
			if (_dead) {
				return false;
			}
			
			//Get position of player's feet
			var floorX:Number = Math.round(this.x / 64);
			var floorY:Number = Math.round(this.y / 64) + 2;
			
			if (!_jumping) {	
				return !this.touchingBlock(floorX, floorY);
			}else {
				return false;
			}
		}
		
		//----------------------------------------------------------Porpertys
		public function get walking():int {
			var head:DisplayObject, feet:DisplayObject;
			
			if(_walking == 0){
				return _walking;
			}else if (_walking == Player.WALK_LEFT) {
				head = map.getMapTile(Math.floor((this.x / 64)
				),
									Math.floor(this.y / 64));
				feet = map.getMapTile(Math.floor((this.x / 64)),
									Math.floor(this.y / 64) + 1);
			}else if (_walking == Player.WALK_RIGHT) {
				head = map.getMapTile(Math.floor((this.x / 64) + 1),
									Math.floor(this.y / 64));
				feet = map.getMapTile(Math.floor((this.x / 64) + 1),
									Math.floor(this.y / 64) + 1);
			}else {
				trace("Why? " + _walking);
				return 0;
			}
			//Head and feet hit?
			var top:Boolean, bottom:Boolean;
			if (head != null) {
				//Collistion detection
				if ( head is com.spikedhand.doodleEscape.Block) {
					top = true;
				}
			}
			if (feet != null) {
				//Collistion detection
				if ( feet is com.spikedhand.doodleEscape.Block) {
					bottom = true;
				}
			}
			
			if ( top || bottom) {
				return 0;
			}
			
			return _walking
		}
		
		public function set walking(value:int):void {
			//Make sure player is told to walk left or right
			if ((value == WALK_LEFT) || (value == WALK_RIGHT) || (value == 0)) {
				_walking = value;
				if (value == 0) {
					//Add standing clip if not
					if (this.getChildAt(0) != stillClip) {
						MovieClip(this.getChildAt(0)).stop();
						this.removeChild(this.getChildAt(0));
						this.addChild(stillClip);
					}
				}else {
					//Add walking clip if not already
					if (this.getChildAt(0) != walkingClip) {
						MovieClip(this.getChildAt(0)).stop();
						this.removeChild(this.getChildAt(0));
						this.addChild(walkingClip);
						MovieClip(this.getChildAt(0)).play();
					}
					
				}
			}else {
				_walking = 0;
				this.dispatchEvent(new ErrorEvent("Invalid Paramater: Bad walking direction"));
			}
		}
		
		public function get startX():Number {
			return _startX;
		}
		
		public function set startX(value:Number):void {
			this._startX = value;
		}
				
		public function set startY(value:Number):void {
			this._startY = value;
		}
		public function get startY():Number {
			return _startY;
		}
	}
}
