package com.spikedhand.doodleEscape
{
	import asset.*;
	import flash.display.DisplayObject;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.KeyboardEvent;
	import flash.media.SoundChannel;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.text.TextFormat;
	import asset.sound.*;
	import flash.events.MouseEvent;	
	/**
	 * ...
	 * @author Devin Handspiker-Wade
	 */
	public class Main extends flash.display.MovieClip 
	{
		
		private var level:int;
		private var isDebug:Boolean;
		private var map:Map;
		private var txtDebug:TextField
		private var txtLives:TextField
		private var lastBlock:DisplayObject;
		private var menu:Array;
		private var fail:Fail;
		private var backgroundSound:CrowdTalking;
		
		public function Main():void 
		{
			//Build sounds
			fail = new Fail();
			backgroundSound = new CrowdTalking();
			
			//Add the background
			this.addChild(new background())
			
			//Keep track of menu options
			menu = new Array();
			//"pool" menu objects
			menu[0] = new Title();
			menu[1] = new BtnStart();
			menu[2] = new Loading();
			menu[3] = new BtnAgain();
			menu[4] = new GameOverText();
			menu[5] = new YouWonText();

			//-------------------------------------------------Setup Menus
			//----------------------------Set positions
			//Title
			menu[0].x = (stage.width / 2) - (menu[0].width/2);
			
			//Start button
			menu[1].x = (stage.width / 2) - (menu[1].width/2);
			menu[1].y = stage.height - 250;
			
			//"pLAY aGIAN" button
			menu[3].x = (stage.width / 2) - (menu[1].width/2);
			menu[3].y = stage.height - (menu[3].height +20);
						
			//Game Over Text
			menu[4].x = (stage.width / 2) - (menu[4].width / 2);
			
			//Win Text
			menu[5].x = (stage.width / 2) - (menu[5].width / 2);
			menu[5].y =(menu[4].height +20);
			
			//--------------------------End positions
			
			//---------------------------Events listeners
			menu[1].addEventListener(MouseEvent.CLICK, function (e:Event):void {
				new MenuClunk().play();
				loadLevel();
				clearMenu();
			});
			
			menu[3].addEventListener(MouseEvent.CLICK, function (e:Event):void {
				new MenuClunk().play();
				//Reset level count
				level = 0;
				loadLevel();
				clearMenu();
			});
			
			//-------------------------End Event listeners
			
			//Start level count
			level = 0;
			
			map = new Map();
			this.addChild(map);
			
			stage.addEventListener(KeyboardEvent.KEY_DOWN, controlsDown);
			stage.addEventListener(KeyboardEvent.KEY_UP, controlsUp);
			
			//Level change event
			this.addEventListener(Exit.LEVEL_DONE, function (e:Event):void {
					loadLevel(); 
			}, true );
			
			//Add GameOver Event
			this.addEventListener("GAMEOVER", gameover, true);
			this.addEventListener(Map.ERROREVENT, gameWin, true);
			this.addEventListener("LEVELLOADED", function (e:Event):void {
				clearMenu();
			}, true);
			
			//Setup first menu
			this.addChild(menu[0]);
			this.addChild(menu[1]);
			
			//Output lives
			txtLives = new TextField();
			txtLives.multiline = false;
			txtLives.width += 30;
			txtLives.height = 25;
			txtLives.background = true;
			txtLives.backgroundColor = 0xEAFCFC;
			//Format the text
			var formatLive:TextFormat = new TextFormat();
			formatLive.bold = true;
            formatLive.color = 0x000f55; 
            formatLive.size = 24; 
 
            txtLives.defaultTextFormat = formatLive;
			txtLives.x = (stage.width / 2) - (txtLives.width / 2)
			txtLives.y += 20;
			stage.addChild(txtLives)
			
			stage.addEventListener(Event.EXIT_FRAME, interfaceFrame);
			
			//Are we in debug
			isDebug = new Error().getStackTrace().search(/:[0-9]+\]$/m) > -1;
			
			trace("Debug= " + isDebug);
			
			if(isDebug){
				//----------------------------------------------------------------------------Debug
				txtDebug = new TextField();
				trace("Created x and Y");
				txtDebug.multiline = true; 
				var format:TextFormat = new TextFormat(); 
				format.color = 0xFF0000; 
				format.size = 16; 
	 
				txtDebug.defaultTextFormat = format; 
				stage.addChild(txtDebug)
				//------------------------------------------------------------------------------END DEBUG
			}
			
			//Start annoying music at half volume
			var sc:SoundChannel = backgroundSound.play(0, int.MAX_VALUE);
		}
		
		private function loadLevel():void {
			map.clear();
			//Loading text
			this.addChild(menu[2]);
			
			map.addByURL( new URLRequest("../level" + level + ".txt"));
			level++;
			stage.focus = map;
			map.lives = 3;
		}
		
		public function clearMenu():void{
			for (var i:int = 0; i < menu.length; i++ ) {
				//If on screen hide
				if (menu[i] != null) {

					if (this.contains(menu[i])) {
						trace(i + "in");
						this.removeChild(menu[i]);					
					}
				}
			}
		}
		
		private function interfaceFrame(e:Event):void {
			if (map.player != null) {
			
				if (isDebug) {
					//--------------------------------------------------------------------------DEBUG
					txtDebug.text = "Player X=" + map.player.x + "\nPlayer Y=" + map.player.y;
					
					if (lastBlock != null) {
						lastBlock.alpha = 1;
					}
				
					lastBlock = map.getMapTile(Math.round(map.player.x / 64), Math.round(map.player.y / 64) + 2)
				
					if (lastBlock != null) {
						lastBlock.alpha = 0.3;
					}
				}
				
				//Should I display the lives?
				var lives:Boolean = true;
				//Check if a menu is on screen
				for each (var thing:DisplayObject in menu ){
					if (stage.contains(thing)) {
						lives = false;
						break;
					}
				}
				
				if (lives) {
					if (!txtLives.visible) {
						txtLives.visible = true;
					}
					txtLives.text = "Lives left: " +map.lives;
				}
				
			}else {	
				if(isDebug){
					txtDebug.text = "Player = NULL";
				}
				//Hide the lives counts
				txtLives.visible = false;
			}
		}
		
		private function controlsDown (ke:KeyboardEvent):void {
				if (ke.keyCode == 72) {
					trace("added Move");
					
				}
				if (map.player != null) {
					
					if(!(map.player.isDead|| map.player.isDying)){
						if (ke.keyCode == 38 || ke.keyCode == 32 ||ke.keyCode == 87) { // up arrow and space
							if(!map.player.isJumping){
								map.player.jump();
							}
						}
						
						//CONTROL WALKING
						if (ke.keyCode == 39|| ke.keyCode == 68) { // right arrow and d
							map.player.walking = Player.WALK_RIGHT;
						}else if (ke.keyCode == 37||ke.keyCode == 65) {//Left and "a"
							map.player.walking = Player.WALK_LEFT;
						}
					}
				}
			}
	
		private function controlsUp (ke:KeyboardEvent):void {
			if (map.player != null) {
				if(!(map.player.isDead|| map.player.isDying)){
					if ((ke.keyCode == 39) || (ke.keyCode == 37)|| ke.keyCode == 68||ke.keyCode == 65) {
						map.player.walking = 0;
					}
				}
			}
		}
		
		private function gameover(e:Event = null):void {
			new ShufflingCards().play();
			trace("Game over triggered");
			map.clear()
			this.addChild(menu[4]);
			this.addChild(menu[3]);
		}
		
		private  function gameWin(e:Event):void {
				clearMenu();
				trace("they won");
				//Add wining text
				this.addChild(menu[5]);
				gameover(e);
			}
	}
	
}