package com.spikedhand.doodleEscape 
{
	import asset.RollingEnemy;
	import flash.events.Event;
	/**
	 * ...
	 * @author ...
	 */
	public class RollingEnemy extends Enemy 
	{
		private var map:Map;
		private var currentDirection:int;
		public function RollingEnemy(map:Map) 
		{
			this.map = map;
			super(map);
			this.addChild(new asset.RollingEnemy());
			this.addEventListener(Event.EXIT_FRAME, collustion);
			
			//Start the rolling
			this.currentDirection = -1;
		}
		
		private function collustion(e:Event):void {
			
			var x:Number = Math.round(this.x / 64),
				y:Number = Math.round(this.y / 64);
				
			//Should we be falling?
			if (this.movingY == 0 && !(map.getMapTile(x, y+1) is Block)) {
				this.verticalMove(1);				
			}
				
			//If no more moving
			if(this.movingX == 0){
				if (map.getMapTile(x + 1, y) is Block) {
					//trace("hit on rigth");
					currentDirection = -1;
				}else if (map.getMapTile(x - 1, y) is Block) {
					//trace("hit on left");
					currentDirection = 1;
				}
				
				this.horizontalMove(currentDirection);
			}
		}
		
	}

}