package com.spikedhand.doodleEscape 
{
	import flash.display.Sprite;
	import flash.events.Event;
	/**
	 * 
	 * @author 
	 */
	public class KillerTile extends Sprite{
		
		private var map:Map;
		
		public function KillerTile(map:Map) {
			super();
			this.map = map;
			
			this.width = 64;
			this.height = 64;
			
		}
		
	}

}